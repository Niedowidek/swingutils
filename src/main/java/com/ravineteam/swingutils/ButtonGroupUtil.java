package com.ravineteam.swingutils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Collection;

public class ButtonGroupUtil {

    public void addToPanel(JPanel buttonPanel, Collection<JRadioButton> buttons) {
        if (buttons != null) {
            addToPanel(buttonPanel, buttons.toArray(new JRadioButton[]{}));
        }
    }

    public void addToPanel(JPanel buttonPanel, JRadioButton... buttons) {
        ButtonGroup mapButtonGroup = new ButtonGroup();
        JPanel buttonGroupPanel = new JPanel();
        for (JRadioButton button : buttons) {
            mapButtonGroup.add(button);
            buttonGroupPanel.add(button);
            buttonGroupPanel.setAlignmentX(SwingConstants.LEFT);
        }
        buttonPanel.add(buttonGroupPanel);
    }


    public void addToPanelVertically(JPanel buttonPanel, Collection<JRadioButton> buttons) {
        if (buttons != null) {
            addToPanelVertically(buttonPanel, buttons.toArray(new JRadioButton[]{}));
        }
    }

    public void addToPanelVertically(JPanel buttonPanel, JRadioButton... buttons) {
        Box box = Box.createVerticalBox();
        ButtonGroup mapButtonGroup = new ButtonGroup();
        JPanel buttonGroupPanel = new JPanel();
        for (JRadioButton button : buttons) {
            mapButtonGroup.add(button);
            buttonGroupPanel.add(button);
            buttonGroupPanel.setAlignmentX(SwingConstants.LEFT);
        }
        for (JRadioButton button : buttons) {
            box.add(button);
            box.add(button);
            box.setAlignmentX(SwingConstants.LEFT);
            box.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        }
        buttonPanel.add(box);
    }

    public JRadioButton setRadioButton(ActionListener actionListener, String label, String optionName) {
        JRadioButton names = new JRadioButton(label);
        names.setActionCommand(optionName);
        names.addActionListener(actionListener);
        return names;
    }
}
