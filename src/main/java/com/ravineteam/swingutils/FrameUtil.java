package com.ravineteam.swingutils;

import javax.swing.*;
import java.awt.*;

public class FrameUtil {

    public static JFrame setupFrame(Container container, String title) {
        return setupFrame(container, title, null);
    }

    public static JFrame setupFrame(Container container, String title, Dimension dimension) {
        JFrame frame = new JFrame(title);
        frame.setContentPane(container);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        if (dimension != null && dimension.getWidth() > 0 && dimension.getHeight() > 0) {
            frame.setPreferredSize(dimension);
        }
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        return frame;
    }

}
