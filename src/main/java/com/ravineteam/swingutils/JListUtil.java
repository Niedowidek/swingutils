package com.ravineteam.swingutils;

import javax.swing.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class JListUtil {

    private JList list;

    public JListUtil(JList list) {
        this.list = list;
    }

    public void refreshList(Object... actualObjects) {
        if (list == null) return;
        DefaultListModel<Object> model = new DefaultListModel<>();
        for (Object object : actualObjects) {
            model.addElement(object);
        }
        this.list.setModel(model);
    }

    public void addToList(Object... objectArgs) {
        Collection<Object> objects = Arrays.asList(objectArgs);
        if (list == null || list.getModel() == null || objects == null || objects.size() == 0) return;
        DefaultListModel<Object> updatedModel = new DefaultListModel<>();
        Set<Object> duplicates = new HashSet<>();
        for (int i = 0; i < list.getModel().getSize(); i++) {
            Object alreadyOnList = list.getModel().getElementAt(i);
            if (alreadyOnList != null) {
                updatedModel.addElement(alreadyOnList);
                for (Object user : objects) {
                    if (alreadyOnList.equals(user)) {
                        duplicates.add(user);
                    }
                }
            }
        }
        objects.removeAll(duplicates);
        for (Object object : objects) {
            if (object != null) {
                updatedModel.addElement(object);
            }
        }
        list.setModel(updatedModel);
    }

    public void removeFromList(Object... objects) {
        if (list == null || list.getModel() == null || objects == null || objects.length == 0) return;
        DefaultListModel<Object> updatedModel = new DefaultListModel<>();
        for (int i = 0; i < list.getModel().getSize(); i++) {
            Object alreadyOnList = list.getModel().getElementAt(i);
            if (alreadyOnList != null) {
                boolean selectedForRemoval = false;
                for (Object object : objects) {
                    if (alreadyOnList.equals(object)) {
                        selectedForRemoval = true;
                        break;
                    }
                }
                if ( ! selectedForRemoval) {
                    updatedModel.addElement(alreadyOnList);
                }
            }
        }
        list.setModel(updatedModel);
    }

}
