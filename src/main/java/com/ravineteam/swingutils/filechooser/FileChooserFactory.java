package com.ravineteam.swingutils.filechooser;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;

public class FileChooserFactory {

    public IFileChooserOption getOptionWithFileListener(JButton button, JTextComponent pathTextArea, String fileDescription, Component component) {
        IFileChooserOption option = new FileChooserOption(button, pathTextArea, fileDescription, JFileChooser.FILES_ONLY);
        FileChooserListenerImpl listener = new FileChooserListenerImpl(option, component);
        option.setFileChooserListener(listener);
        return option;
    }

}
