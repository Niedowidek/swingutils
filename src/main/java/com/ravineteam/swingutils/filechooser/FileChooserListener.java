package com.ravineteam.swingutils.filechooser;

import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Optional;

public interface FileChooserListener extends ActionListener {

    Collection<FileChooserOptionProcessor> getProcessors();

    boolean isProcessingSuccessful();

    Optional<FileChooserOptionProcessor> getFailedProcessor();
}
