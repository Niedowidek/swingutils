package com.ravineteam.swingutils.filechooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;

public class FileChooserListenerImpl implements FileChooserListener {

    private Component component;
    private IFileChooserOption option;
    private Collection<FileChooserOptionProcessor> processors;
    private boolean processingResult;
    private FileChooserOptionProcessor failedProcessor;

    public FileChooserListenerImpl(IFileChooserOption option, Component component) {
        this.component = component;
        this.option = option;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (option != null && option.getFileChooser() != null && e.getSource() == option.getButton()) {
            int returnVal = option.getFileChooser().showOpenDialog(component);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                option.refreshPath();
                processFileChooserOption(option);
            }
        }
    }

    protected boolean processFileChooserOption(IFileChooserOption option) {
        this.processingResult  = true;
        if (getProcessors().size() > 0 && option != null) {
            for (FileChooserOptionProcessor processor : processors) {
                if (processor != null) {
                    if ( ! processor.process(option)) {
                        this.processingResult  = false;
                        this.failedProcessor = processor;
                        break;
                    }
                }
            }
        }
        return this.processingResult ;
    }

    @Override
    public Collection<FileChooserOptionProcessor> getProcessors() {
        if (processors == null) {
            processors = new LinkedList<>();
        }
        return processors;
    }

    @Override
    public boolean isProcessingSuccessful() {
        return processingResult;
    }

    @Override
    public Optional<FileChooserOptionProcessor> getFailedProcessor() {
        return Optional.ofNullable(failedProcessor);
    }
}
