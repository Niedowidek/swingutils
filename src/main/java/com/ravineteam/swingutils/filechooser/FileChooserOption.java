package com.ravineteam.swingutils.filechooser;

import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class FileChooserOption implements IFileChooserOption {

    private JFileChooser fileChooser;
    private JButton button;
    private String fileDescription;
    private JTextComponent path;
    private FileChooserListener listener;

    public FileChooserOption(String buttonLabel, String fileDescription, int fileSelectionMode) {
        this(new JButton(buttonLabel), fileDescription, fileSelectionMode);
    }

    public FileChooserOption(JButton button, String fileDescription, int fileSelectionMode) {
        init(button, new JTextArea(), fileDescription, fileSelectionMode);
    }

    public FileChooserOption(JButton button, JTextComponent pathTextArea, String fileDescription, int fileSelectionMode) {
        init(button, pathTextArea, fileDescription, fileSelectionMode);
    }

    private void init(JButton button, JTextComponent pathTextArea, String fileDescription, int fileSelectionMode) {
        this.button = button;
        this.fileDescription = fileDescription != null ? fileDescription : "";
        this.fileChooser = new JFileChooser();
        this.path = pathTextArea;
        try {
            this.fileChooser.setFileSelectionMode(fileSelectionMode);
        } catch (IllegalArgumentException e) {
            this.fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        }
    }

    @Override
    public void setFileChooserListener(FileChooserListener listener) {
        this.listener = listener;
        clearButtonListeners();
        this.button.addActionListener(listener);
    }

    private void clearButtonListeners() {
        List<ActionListener> toRemove = new LinkedList<>();
        for (ActionListener actionListener : button.getActionListeners()) {
            toRemove.add(actionListener);
        }
        if (toRemove.size() > 0) {
            for (ActionListener listenerToRemove : toRemove) {
                button.removeActionListener(listenerToRemove);
            }
        }
    }

    @Override
    public Optional<FileChooserListener> getListener() {
        return Optional.ofNullable(listener);
    }

    @Override
    public JFileChooser getFileChooser() {
        if (fileChooser == null) fileChooser = new JFileChooser();
        return fileChooser;
    }

    @Override
    public JButton getButton() {
        return button;
    }

    @Override
    public String getFileDescription() {
        return fileDescription;
    }

    @Override
    public JTextComponent getPath() {
        refreshPath();
        return path;
    }

    @Override
    public void refreshPath() {
        this.path.setText(this.getPathText());
    }

    @Override
    public String getPathText() {
        String description = "";
        if (this.getFileChooser().getSelectedFile() == null ) {
            description = "not chosen";
        } else if (!this.getFileChooser().getSelectedFile().exists()) {
            description = "chosen path doesn't exist (error)";
        } else {
            description = this.getFileChooser().getSelectedFile().getAbsolutePath();
        }
        return StringUtils.capitalize(this.getFileDescription()) + ": " + description;
    }

    @Override
    public String getSelectedFileAbsolutePath() {
        if (this.getFileChooser() != null && this.getFileChooser().getSelectedFile() != null && this.getFileChooser().getSelectedFile().getAbsolutePath() != null) {
            return this.getFileChooser().getSelectedFile().getAbsolutePath();
        }
        return "";
    }

    @Override
    public boolean isNotEmpty() {
        return this.getButton() != null && this.getFileChooser() != null && StringUtils.isNotEmpty(this.getFileDescription());
    }
}
