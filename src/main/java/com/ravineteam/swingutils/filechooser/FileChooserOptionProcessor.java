package com.ravineteam.swingutils.filechooser;

public interface FileChooserOptionProcessor {

    boolean process(IFileChooserOption option);
}
