package com.ravineteam.swingutils.filechooser;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.util.Optional;

public interface IFileChooserOption {
    void setFileChooserListener(FileChooserListener listener);

    Optional<FileChooserListener> getListener();

    JFileChooser getFileChooser();

    JButton getButton();

    String getFileDescription();

    JTextComponent getPath();

    void refreshPath();

    String getPathText();

    String getSelectedFileAbsolutePath();

    boolean isNotEmpty();
}
